package com.tool.sqlpad.controller;

import com.tool.sqlpad.model.Query;
import com.tool.sqlpad.service.SqlService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/query")
public class MainController {
    private final SqlService sqlService;

    public MainController(SqlService sqlService) {
        this.sqlService = sqlService;
    }

    @PostMapping
    public Object getResult(@RequestBody Query query) {
        try {
            return sqlService.query(query.getQuery());
        } catch (Exception exception) {
            return exception;
        }
    }
}
